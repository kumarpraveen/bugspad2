# -*- coding: utf-8 -*-
#
# Copyright © 2014  Kushal Das <kushaldas@gmail.com>
# Copyright © 2014  Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions
# of the GNU General Public License v.2, or (at your option) any later
# version.  This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.  You
# should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

'''
Bugspad main flask controller.
'''

__requires__ = ['SQLAlchemy >= 0.7', 'jinja2 >= 2.4']
import pkg_resources

import logging
import logging.handlers
import os
import sys
from pprint import pprint

import flask
from flask import request


from functools import wraps
from sqlalchemy.exc import SQLAlchemyError
from api import API

__version__ = '0.3'

APP = flask.Flask(__name__)
APP.register_blueprint(API)

APP.config.from_object('bugspad2.default_config')
if 'MM2_CONFIG' in os.environ:  # pragma: no cover
    APP.config.from_envvar('MM2_CONFIG')



#if APP.config.get('MM_AUTHENTICATION') == 'fas':
#    # Use FAS for authentication
#    from flask.ext.fas_openid import FAS
#    FAS = FAS(APP)


# Points the template and static folders to the desired theme
APP.template_folder = os.path.join(
    APP.template_folder, APP.config['THEME_FOLDER'])
APP.static_folder = os.path.join(
    APP.static_folder, APP.config['THEME_FOLDER'])


# Set up the logger
# Send emails for big exception
MAIL_HANDLER = logging.handlers.SMTPHandler(
    APP.config.get('SMTP_SERVER', '127.0.0.1'),
    'nobody@fedoraproject.org',
    APP.config.get('MAIL_ADMIN', 'noreply@fedoraproject.org'),
    'MirrorManager2 error')
MAIL_HANDLER.setFormatter(logging.Formatter('''
    Message type:       %(levelname)s
    Location:           %(pathname)s:%(lineno)d
    Module:             %(module)s
    Function:           %(funcName)s
    Time:               %(asctime)s

    Message:

    %(message)s
'''))
MAIL_HANDLER.setLevel(logging.ERROR)
#if not APP.debug:
#    APP.logger.addHandler(MAIL_HANDLER)

# Log to stderr as well
STDERR_LOG = logging.StreamHandler(sys.stderr)
STDERR_LOG.setLevel(logging.INFO)
APP.logger.addHandler(STDERR_LOG)

LOG = APP.logger


import bugspad2
import bugspad2.lib as mmlib
import bugspad2.forms as forms
import bugspad2.lib.model as model


SESSION = mmlib.create_session(APP.config['DB_URL'])
#mmlib.load_all(SESSION)


def is_authenticated():
    """ Returns whether the user is currently authenticated or not. """
    return hasattr(flask.g, 'fas_user') and flask.g.fas_user is not None


def login_required(function):
    """ Flask decorator to ensure that the user is logged in. """
    @wraps(function)
    def decorated_function(*args, **kwargs):
        ''' Wrapped function actually checking if the user is logged in.
        '''
        if not is_authenticated():
            return flask.redirect(flask.url_for(
                'auth_login', next=flask.request.url))
        return function(*args, **kwargs)
    return decorated_function

# # Flask application

@APP.context_processor
def inject_variables():
    """ Inject some variables into every template.
    """
    return dict(
        version=__version__
    )

@APP.route('/')
def index():
    """ Displays the index page.
    """

    return flask.render_template(
        'index.html',
    )

@APP.route('/products/')
@login_required
def select_product():
    """Allows user to select the product.
Next page has the actual bug form."""
    if request.method == 'GET':
        products = mmlib.get_products()
        return flask.render_template(
            'products.html',
            products=products
        )

@APP.route('/bugs/new/<int:product_id>', methods=['GET',])
@login_required
def new_bug(product_id):
    """Form to file a new bug."""
    if request.method == 'GET':
        form = forms.NewBugForm()
        form.bug_component.choices = mmlib.get_components(product_id)
        versions = mmlib.get_versions(product_id)
        form.bug_versions.choices = versions
        form.bug_severity.choices = [('blocker','blocker'), ('critical', 'critical'),
                                     ('trivial', 'trivial'), ('enhancement', 'enhancement'), ('normal', 'normal')]
        form.bug_status.choices = [('new','new'), ('closed','closed')]
        return flask.render_template(
            'newbug.html',
            form=form,
            product_id=product_id
        )


@APP.route('/bugs/new/', methods=['POST',])
@login_required
def file_bug():
    """Saves a new bug."""
    if request.method == 'POST':
        form = forms.NewBugForm()
        print form.product_id.data
        form.bug_component.choices = mmlib.get_components(form.product_id.data)
        versions = mmlib.get_versions(form.product_id.data)
        form.bug_versions.choices = versions
        form.bug_severity.choices = [('blocker','blocker'), ('critical', 'critical'),
                                     ('trivial', 'trivial'), ('enhancement', 'enhancement'), ('normal', 'normal')]
        form.bug_status.choices = [('new','new'), ('closed','closed')]
        form.bug_priority.choices = [('high', 'High'), ('low', 'Low'), ('medium', 'Medium'), ('critical', 'Critical')]
        if form.validate_on_submit():
            bug_id = mmlib.save_bug_form(SESSION, form, flask.g.fas_user.id)
            # We should sent the user to the view bug page.
            return flask.redirect(flask.url_for('view_bug', bug_id=bug_id))
        else:
            print form.errors
            print form.bug_versions.data, form.bug_component.data
            return "Error there. Ask the sysadmin to check the log."


@APP.route('/bugs/<int:bug_id>', methods=['POST','GET'])
def view_bug(bug_id):
    bug_id = str(bug_id)
    if request.method == 'GET':
        return view_bug_helper(bug_id)
    elif request.method == 'POST':
        if not is_authenticated():
            form = forms.LoginForm()
            return flask.render_template(
                'login.html',
                form=form,
            )
        # Now it is a POST and authenticated user.
        # TODO:
        # Check each field from the form and if required
        # update the db first and then the redis indexes.
        # HEXISTS, HGET and HSET are the commands.
        comment = unicode(request.form['new_comment']).strip()
        if comment != '':
            mmlib.add_comment(SESSION, bug_id, comment, flask.g.fas_user.id)
        return view_bug_helper(bug_id)


def view_bug_helper(bug_id):
    form = forms.NewBugForm()
    bugdata = mmlib.get_bug(bug_id)
    components = mmlib.get_components(bugdata['product_id'])
    versions = mmlib.get_versions(bugdata['product_id'])
    severity = [('normal', 'Normal'),('blocker','Blocker'), ('critical', 'Critical'),
                ('trivial', 'Trivial'), ('enhancement', 'Enhancement'), ]
    priority = [('high', 'High'), ('low', 'Low'), ('medium', 'Medium'), ('critical', 'Critical')]
    status = [('new','New'), ('closed','Closed')]
    comments = mmlib.get_comments(SESSION, bug_id)
    component_name = mmlib.get_component_name(bugdata['component'])
    auth_flag = is_authenticated()
    return flask.render_template(
        'editbug.html',
        form=form,
        bd=bugdata,
        components=components,
        versions=versions,
        severity=severity,
        priority=priority,
        status=status,
        auth_flag=auth_flag,
        comments=comments,
        component_name=component_name
    )


@APP.route('/login', methods=['GET', 'POST'])
def auth_login():  # pragma: no cover
    """ Login mechanism for this application.
    """
    next_url = flask.url_for('index')
    if 'next' in flask.request.values:
        next_url = flask.request.values['next']

    if next_url == flask.url_for('auth_login'):
        next_url = flask.url_for('index')

    if APP.config.get('MM_AUTHENTICATION', None) == 'fas':
        if hasattr(flask.g, 'fas_user') and flask.g.fas_user is not None:
            return flask.redirect(next_url)
        else:
            return FAS.login(return_url=next_url)
    elif APP.config.get('MM_AUTHENTICATION', None) == 'local':
        form = forms.LoginForm()
        return flask.render_template(
            'login.html',
            next_url=next_url,
            form=form,
        )

@APP.route('/logout')
def auth_logout():
    """ Log out if the user is logged in other do nothing.
    Return to the index page at the end.
    """
    next_url = flask.url_for('index')

    if APP.config.get('MM_AUTHENTICATION', None) == 'fas':
        if hasattr(flask.g, 'fas_user') and flask.g.fas_user is not None:
            FAS.logout()
            flask.flash("You are no longer logged-in")
    elif APP.config.get('MM_AUTHENTICATION', None) == 'local':
        login.logout()
    return flask.redirect(next_url)


# Only import the login controller if the app is set up for local login
if APP.config.get('MM_AUTHENTICATION', None) == 'local':
    import bugspad2.login
    APP.before_request(login._check_session_cookie)
    APP.after_request(login._send_session_cookie)

