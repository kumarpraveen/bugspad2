# -*- coding: utf-8 -*-
#
# Copyright © 2014  Kushal Das <kushaldas@gmail.com>
# Copyright © 2014  Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions
# of the GNU General Public License v.2, or (at your option) any later
# version.  This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.  You
# should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

__requires__ = ['SQLAlchemy >= 0.7', 'jinja2 >= 2.4']
import pkg_resources

import hashlib
import datetime

import flask
import json
from flask import Blueprint, request
try:
    from flask.ext.admin.contrib.sqla import ModelView
except ImportError:
    # The module was renamed in flask-admin
    from flask.ext.admin.contrib.sqlamodel import ModelView
from sqlalchemy.exc import SQLAlchemyError

import bugspad2.forms
import bugspad2.lib as mmlib
from bugspad2.lib import model
from bugspad2.default_config import  DB_URL

API = Blueprint('api',__name__)
session = mmlib.create_session(DB_URL)

@API.route('/api/bug/new', methods=['POST',])
def api_new_bug():
    if request.method == 'POST':
        data = json.loads(request.data)
        # First let us see if all required fields are there or not.
        for key in ['user', 'password', 'summary', 'description', 'component_id', 'product_id', 'product_version']:
            if key not in data:
                return "Data missing %s" % key
        # Let us convert everything into str.
        for k, v in data.iteritems():
            data[k] = unicode(v)
        user_id = mmlib.redis_auth(data['user'], data['password'])
        if user_id:
            bugd = {}
            bugd['summary'] = data['summary']
            bugd['desc'] = data['description']
            bugd['component'] = data['component_id']
            bugd['bug_versions'] = data['product_version']
            bugd['product_id'] = data['product_id']
            # Optional items
            bugd['severity'] = data.get('severity','normal')
            bugd['priority'] = data.get('priority','low')
            bugd['bug_status'] = data.get('bug_status', 'new')
            bugd['bug_whiteboard'] = data.get('bug_whiteboard', '')
            bugd['bug_hardware'] = data.get('bug_hardware', '')
            bugd['bug_assignee'] = data.get('bug_assignee', '')
            bugd['bug_depends_on'] = data.get('bug_depends_on', '')
            bugd['bug_blocks_on'] = data.get('bug_blocks_on', '')
            bugd['bug_cc'] = data.get('bug_cc', '')
            bugd['bug_docs'] = data.get('bug_docs', '')
            return unicode(mmlib.save_bug(session, bugd, user_id))
        else:
            return "Authentication problem."

@API.route('/api/comments/new', methods=['POST',])
def api_new_comment():
    if request.method == 'POST':
        data = json.loads(request.data)
        # First let us see if all required fields are there or not.
        for key in ['user', 'password', 'msg', 'bug_id']:
            if key not in data:
                return "Data missing %s" % key
        # Let us convert everything into str.
        for k, v in data.iteritems():
            data[k] = unicode(v)
        user_id = mmlib.redis_auth(data['user'], data['password'])
        if user_id:
            mmlib.add_comment(session, data['bug_id'], data['msg'], user_id)
            return "Comment added."
        else:
            return "Authentication problem."