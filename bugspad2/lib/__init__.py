# -*- coding: utf-8 -*-
#
# Copyright © 2014  Kushal Das <kushaldas@gmail.com>
# Copyright © 2014  Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions
# of the GNU General Public License v.2, or (at your option) any later
# version.  This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.  You
# should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

'''
Bugspad internal api.
'''
__requires__ = ['SQLAlchemy >= 0.7', 'jinja2 >= 2.4']
import pkg_resources

import datetime
import random
import string
import json
import hashlib
from datetime import datetime

from redis import Redis
redis = Redis()

import sqlalchemy

from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import SQLAlchemyError

from bugspad2.lib import model
from bugspad2.lib import notifications
from bugspad2 import default_config



def create_session(db_url, debug=False, pool_recycle=3600):
    ''' Create the Session object to use to query the database.

    :arg db_url: URL used to connect to the database. The URL contains
    information with regards to the database engine, the host to connect
    to, the user and password and the database name.
      ie: <engine>://<user>:<password>@<host>/<dbname>
    :kwarg debug: a boolean specifying wether we should have the verbose
        output of sqlalchemy or not.
    :return a Session that can be used to query the database.

    '''
    engine = sqlalchemy.create_engine(
        db_url, echo=debug, pool_recycle=pool_recycle)
    scopedsession = scoped_session(sessionmaker(bind=engine))
    return scopedsession

def get_user_by_username(session, username):
    ''' Return a specified User via its username.

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.User
    ).filter(
        model.User.user_name == username
    )

    return query.first()


def get_user_by_email(session, email):
    ''' Return a specified User via its email address.

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.User
    ).filter(
        model.User.email_address == email
    )

    return query.first()


def get_user_by_token(session, token):
    ''' Return a specified User via its token.

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.User
    ).filter(
        model.User.token == token
    )

    return query.first()


def get_session_by_visitkey(session, sessionid):
    ''' Return a specified VisitUser via its session identifier (visit_key).

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.UserVisit
    ).filter(
        model.UserVisit.visit_key == sessionid
    )

    return query.first()

def id_generator(size=15, chars=string.ascii_uppercase + string.digits):
    """ Generates a random identifier for the given size and using the
    specified characters.
    If no size is specified, it uses 15 as default.
    If no characters are specified, it uses ascii char upper case and
    digits.
    :arg size: the size of the identifier to return.
    :arg chars: the list of characters that can be used in the
        idenfitier.
    """
    return ''.join(random.choice(chars) for x in range(size))


def load_all(session):
    '''Load all details as required into redis

    :arg session: the session with which to connect to the database.
    '''
    query = session.query(model.User)
    for row in query:
        redis.hset("users", row.user_name, row.id)
        redis.hset("usersid", row.id, row.user_name)
        redis.hset('userpass', row.user_name, row.password)
    print "Users loaded in redis."

    productlist = []
    query = session.query(model.Product)
    for row in query:
        result = []
        for ver in row.versions:
            if ver.active == True:
                result.append((ver.id, ver.name))
        data = json.dumps(result)
        redis.set("product:%s" % str(row.id), data)
        productlist.append((row.id, row.name))
        query2 = session.query(model.Component).filter(model.Component.product_id==row.id)
        component_list = []
        for row2 in query2:
            component_list.append((row2.id, row2.name, row2.description))
            redis.hset('componenthash',str(row2.id), row2.name ) # For id->name in components.
        redis.set('componentlist:%s' % str(row.id), json.dumps(component_list))

    redis.set('productlist', json.dumps(productlist))
    print "Products are loaded in memory."


def get_products():
    "Returns the list of the products from redis."
    try:
        return json.loads(redis.get('productlist'))
    except Exception:
        #TODO: Log the error
        pass
    return []

def get_versions(product_id):
    "Returns the list of id,versions for a given product."
    data = json.loads(redis.get('product:%s' % str(product_id)))
    return [(unicode(word[0]), unicode(word[1])) for word in data] #unicode is required for form validation.


def get_components(product_id):
    'Returns the list of id,names for a given product.'
    data = json.loads(redis.get('componentlist:%s' % str(product_id)))
    return [(unicode(word[0]), unicode(word[1])) for word in data] #unicode is required for form validation.


def save_bug_form(session, form, user_id):
    '''Saves the bug from a webform.

    :arg form: wtform instance.
    '''
    bugd = {}
    bugd['summary'] = form.summary.data
    bugd['desc'] = form.bug_desc.data
    bugd['component'] = form.bug_component.data
    bugd['severity'] = form.bug_severity.data
    bugd['priority'] = form.bug_priority.data
    bugd['bug_status'] = form.bug_status.data
    bugd['bug_whiteboard'] = form.bug_whiteboard.data
    bugd['bug_versions'] = form.bug_versions.data
    bugd['bug_hardware'] = form.bug_hardware.data
    bugd['bug_assignee'] = form.bug_assignee.data
    bugd['bug_depends_on'] = form.bug_depends_on.data
    bugd['bug_blocks_on'] = form.bug_blocks_on.data
    bugd['bug_cc'] = form.bug_cc.data
    bugd['bug_docs'] = form.bug_docs.data
    bugd['product_id'] = form.product_id.data
    return save_bug(session, bugd, user_id)

def save_bug(session, bug, user_id):
    'Saves the bug in database and redis.'
    newbug = model.Bugs()
    newbug.summary = bug['summary']
    newbug.description = bug['desc']
    newbug.component_id = bug['component']
    newbug.status = bug['bug_status']
    newbug.severity = bug['severity']
    newbug.whiteboard = bug['bug_whiteboard']
    newbug.version = bug['bug_versions']
    newbug.hardware = bug['bug_hardware']
    newbug.assignee = bug['bug_assignee']
    newbug.depends_on = bug['bug_depends_on']
    newbug.blocks_on = bug['bug_blocks_on']
    newbug.cc = bug['bug_cc']
    newbug.bug_docs = bug['bug_docs']
    newbug.product_id = bug['product_id']
    newbug.reported = datetime.now()
    newbug.reporter = user_id

    #try:
    session.add(newbug)
    session.commit()

        #Now save in redis.
    bug['id'] = newbug.id
    redis.hset('bugs', str(bug['id']), json.dumps(bug))
    redis.hset('bcomponent:' + bug['component'], bug['component'], str(bug['id']))
    redis.hset('bproduct:' + bug['product_id'], bug['product_id'], str(bug['id']))
    redis.hset('bstatus:' + bug['bug_status'], bug['bug_status'], str(bug['id']))
    redis.hset('bseverity:' + bug['severity'], bug['severity'], str(bug['id']))
    redis.hset('bversion:' + bug['bug_versions'], bug['bug_versions'], str(bug['id']))
    #except Exception, err:
    #    print err
    #    return err.message
    return newbug.id

def get_bug(bug_id):
    bug_id = str(bug_id)
    if redis.hexists('bugs', bug_id):
        data = redis.hget('bugs', bug_id)
        return json.loads(data)


def redis_auth(username, password):
    password = '%s%s' % (password, default_config.PASSWORD_SEED)
    password = hashlib.sha512(password).hexdigest()
    if redis.hexists('userpass', username):
        if unicode(password) == redis.hget('userpass', username):
            return redis.hget('users', username)
    return False


def add_comment(sesion, bug_id, comment, user_id):
    """

    :param sesion: Session object for database.
    :param bug_id: ID of the bug against which we are filling the comment.
    :param comment: The comment text in unicode.
    :param user_id: ID of the user who is making comment.

    :return: None or error message
    """
    com = model.Comments(bug_id=bug_id, mesg=comment, commenter=user_id,
                         reported=datetime.now())
    try:
        sesion.add(com)
        sesion.commit()
    except Exception, err:
        print err
        return err


def get_comments(session, bug_id):
    """

    :param session: Session object for database.
    :param bug_id: ID of the bug for which we want to view the comments.
    :return: A list of comments.
    """

    return session.query(model.Comments).filter(model.Comments.bug_id==bug_id)


def get_component_name(component_id):
    '''

    :param component_id: ID of the component.
    :return: Name of the component.
    '''
    return redis.hget('componenthash', component_id)