Installation
==================================

Requirements
-------------

* flask
* flask-admin
* sqlalchemy
* python-redis
* redis
* albemic


Setting up a devel instance
---------------------------

Create a localconfig.py to have the following for SMTP
::

		user='username'
		password='smtp_password'

Start the redis server
----------------------
::

		# service redis start

Database creation
-------------------
::

		$ python createdb.py

The above command will create the database schema as required.


Start the server
-----------------
::

		$ python runserver.py


Create the first user
----------------------

Go to the webbrowser and click on login and then create account to create the first
account in the system.

Add the first product and a version
------------------------------------

Start the repl.py application. *addproduct* and *addversion* commands will
help you to add the first few products and versions.

After that you can use *tempcomponents* command to load the temporary components.
You need to download `comps.json <https://kushal.fedorapeople.org/comps.json.tar.gz>`_
for the same.
