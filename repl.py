#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014  Kushal Das <kushaldas@gmail.com>
# Copyright © 2014  Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions
# of the GNU General Public License v.2, or (at your option) any later
# version.  This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.  You
# should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

__requires__ = ['SQLAlchemy >= 0.7', 'jinja2 >= 2.4']
import pkg_resources
import json
from cmd2 import Cmd
from redis import Redis
from bugspad2.lib import model
import bugspad2.lib as mmlib
from bugspad2.default_config import DB_URL
SESSION = mmlib.create_session(DB_URL)


class REPL(Cmd):
    prompt = 'bp-rpel> '


    def __init__(self):
        self.r = Redis()
        Cmd.__init__(self)

    def do_EOF(self, line):
        'Exit for the tutorial by pressing Ctrl+d'
        print ''
        return True

    def do_addproduct(self, line):
        name = raw_input("Enter product name: ")
        name = name.strip()
        desc = raw_input("Enter product description: ")
        desc = desc.strip()
        pr = model.Product(name=name, description=desc)
        SESSION.add(pr)
        SESSION.commit()
        print pr

    def do_addversion(self, line):
        name = raw_input("Enter version name: ")
        name = name.strip()
        pr_id = raw_input("Enter product id: ")
        pr_id = pr_id.strip()
        ver = model.Version(name=name, product_id=pr_id, active=True)
        SESSION.add(ver)
        SESSION.commit()
        data = json.loads(self.r.get('product:%s' % pr_id))
        data.append((ver.id, name))
        self.r.set('product:%s' % str(pr_id), json.dumps(data))
        print ver


    def do_list(self, line):
        'List any particular item'
        line = line.strip()
        if line == 'product':
            self.list_products()

    def do_loadall(self, line):
        mmlib.load_all(SESSION)


    def list_products(self):
        for row in SESSION.query(model.Product):
            print row.id, row.name, row.description

    def do_tempcomponents(self, line):
        f = open('comps.json')
        comps = json.load(f)
        f.close()
        for comp in comps:
            c = model.Component(name=comp['name'], description=comp['description'], product_id=1)
            SESSION.add(c)
            SESSION.commit()

if __name__ == '__main__':
    REPL().cmdloop()